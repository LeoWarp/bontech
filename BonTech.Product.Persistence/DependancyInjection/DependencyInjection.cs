using BonTech.Product.Domain.Entity;
using BonTech.Product.Domain.Interfaces;
using BonTech.Product.Persistence.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BonTech.Product.Persistence.DependancyInjection
{
    /// <summary>
    /// Внедрение зависимостей слоя Persistence.
    /// </summary>
    public static class DependencyInjection
    {
        public static void AddPersistence(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("PostgreSQL");
            services.AddDbContext<AppDbContext>(options => { options.UseNpgsql(connectionString); });
            
            services.RepositoriesInit();
        }

        public static void RepositoriesInit(this IServiceCollection services)
        {
            services.AddScoped<IBaseRepository<Domain.Entity.Product>, BaseRepository<Domain.Entity.Product>>();
            services.AddScoped<IBaseRepository<Category>, BaseRepository<Category>>();
        }
    }
}