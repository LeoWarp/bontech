﻿using BonTech.Product.Domain.Entity;
using Microsoft.EntityFrameworkCore;

namespace BonTech.Product.Persistence;

public sealed class AppDbContext : DbContext
{
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
    {
        Database.EnsureCreated();
    }
    
    public DbSet<Domain.Entity.Product> Products { get; set; }
    
    public DbSet<Category> Categories { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Domain.Entity.Product>(builder =>
        {
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Name).HasColumnName("Name").HasMaxLength(500).IsRequired();
            builder.Property(x => x.Description).HasColumnName("Description").IsRequired();
            builder.Property(x => x.Price).HasColumnName("Price").IsRequired();

            builder.HasData(new List<Domain.Entity.Product>()
            {
                new()
                {
                    Id = 1,
                    Name = "Боксерские перчатки",
                    Description = "Перчатки хорошего качества",
                    Price = 5000,
                    CategoryId = 3
                },
                new()
                {
                    Id = 2,
                    Name = "Кроссовки для бега",
                    Description = "Классные кроссовки",
                    Price = 300,
                    CategoryId = 1
                }
            });
        });
        
        modelBuilder.Entity<Category>(builder =>
        {
            builder.HasIndex(u => u.Name).IsUnique();
            
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Name).HasColumnName("Name").HasMaxLength(300).IsRequired();

            builder.HasOne(e => e.Product)
                .WithOne(e => e.Category);

            builder.HasData(new List<Category>()
            {
                new()
                {
                    Id = 1,
                    Name = "Спортивная одежда"
                },
                new()
                {
                    Id = 2,
                    Name = "Спортивное питание"
                },
                new()
                {
                    Id = 3,
                    Name = "Оборудование для спорта"
                }
            });
        });
    }
}