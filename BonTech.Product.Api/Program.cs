using System.Reflection;
using BonTech.Product.Api;
using BonTech.Product.Application.DependancyInjection;
using BonTech.Product.Application.Validations;
using BonTech.Product.Application.Validations.FluentValidations;
using BonTech.Product.Domain.Dto;
using BonTech.Product.Domain.Interfaces.Validations;
using BonTech.Product.Persistence.DependancyInjection;
using FluentValidation;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

builder.Services.Configure<JwtSettings>(builder.Configuration.GetSection(JwtSettings.DefaultSection));
builder.Services.AddCors();

builder.Host.UseSerilog((context, configuration) => configuration.ReadFrom.Configuration(context.Configuration));
builder.Services.AddControllers();

builder.Services.AddPersistence(builder.Configuration);
builder.Services.AddApplication(builder.Configuration);

builder.Services.AddSwagger();
//builder.Services.AddAuthenticationAndAuthorization(builder);

builder.Services.AddScoped<IValidator<ProductDto>, CreateValidator>();
builder.Services.AddScoped<IValidator<ProductDto>, UpdateValidator>();
builder.Services.AddScoped<IProductValidator, ProductValidator>();
builder.Services.AddValidatorsFromAssemblies(new [] { Assembly.GetExecutingAssembly() });


var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        c.SwaggerEndpoint($"/swagger/v1/swagger.json", "Awesome CMS Core API V1");
        c.SwaggerEndpoint($"/swagger/v2/swagger.json", "Awesome CMS Core API V2");
        c.RoutePrefix = string.Empty;
    });
}

app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
app.UseSerilogRequestLogging();

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();