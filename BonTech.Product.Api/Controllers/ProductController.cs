using BonTech.Product.Application.Validations.FluentValidations;
using BonTech.Product.Domain.Dto;
using BonTech.Product.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace BonTech.Product.Api.Controllers;
 
//[Authorize]
[ApiController]
[ApiVersion("1.0")]
[ApiExplorerSettings(GroupName = "v1")]
[Produces("application/json")]
[Route("api/v{version:apiVersion}/[controller]")]
public class ProductController : ControllerBase
{
    private readonly IProductService _productService;

    public ProductController(IProductService productService)
    {
        _productService = productService;
    }
    
    /// <summary>
    /// Получение списка всех продуктов в БД
    /// </summary>
    /// <returns></returns>
    [HttpGet("get-products")]
    [ProducesResponseType(typeof(IEnumerable<ProductDto>),StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(IEnumerable<ProductDto>),StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> GetProducts()
    {
        var response = await _productService.GetProductsAsync();
        if (response.IsSuccess)
        {
            return Ok(response);
        }
        return BadRequest(response.ErrorCode);
    }
    
    /// <summary>
    /// Получение списка продуктов из БД по списку названий
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPost("get-products-for-order")]
    [ProducesResponseType(typeof(IEnumerable<OrderProductDto>),StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(IEnumerable<OrderProductDto>),StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> GetProducts(OrderDto dto)
    {
        var response = await _productService.GetProductsAsync(dto);
        if (response.IsSuccess)
        {
            return Ok(response.Data);
        }
        return BadRequest(response.ErrorCode);
    }
    
    /// <summary>
    /// Получение продукта по Id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    /// <response code="200">Возвращается полученный продукт</response>
    /// <response code="400">Если продукт был не найден</response>
    [HttpGet("get-product/{id}")]
    [ProducesResponseType(typeof(ProductDto), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ProductDto),StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> GetProduct(long id)
    {
        var response = await _productService.GetProductAsync(id);
        if (response.IsSuccess)
        {
            return Ok(response);
        }
        return BadRequest(response.ErrorCode);
    }
    
    /// <summary>
    /// Создание продукта с указанием основных свойств и категорий
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Todo
    ///     {
    ///        "name": "Item #1",
    ///        "description": "Test description",
    ///        "price": 200,
    ///        "categories": [ string - name ]
    ///     }
    ///
    /// </remarks>
    /// <response code="200">Возвращается полученный продукт</response>
    /// <response code="400">Если продукт был не найден</response>
    [HttpPost("create")]
    [ProducesResponseType(typeof(ProductDto), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ProductDto), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> CreateProduct([FromBody] ProductDto dto)
    {
        var createProductValidator = new CreateValidator();
        var result = createProductValidator.Validate(dto);
        if (!result.IsValid)
        {
            return BadRequest(string.Join(';', result.Errors));
        }
        
        var response = await _productService.CreateProductAsync(dto);
        if (response.IsSuccess)
        {
            return Ok(response);
        }
        return BadRequest(response);
    }
    
    /// <summary>
    /// Обновление продукта по Id
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPost("update")]
    [ProducesResponseType(typeof(ProductDto), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ProductDto), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> UpdateProduct([FromBody] ProductDto dto)
    {
        var updateProductValidator = new UpdateValidator();
        var result = updateProductValidator.Validate(dto);
        if (!result.IsValid)
        {
            return BadRequest(string.Join(';', result.Errors));
        }
        
        var response = await _productService.UpdateProductAsync(dto);
        if (response.IsSuccess)
        {
            return Ok(response);
        }
        return BadRequest(response);
    }
    
    /// <summary>
    /// Удаление продукта по Id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpPost("delete")]
    [ProducesResponseType(typeof(bool), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(bool), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> DeleteProduct(long id)
    {
        var response = await _productService.DeleteProductAsync(id);
        if (response.IsSuccess)
        {
            return Ok(response);
        }
        return BadRequest(response.ErrorCode);
    }
}