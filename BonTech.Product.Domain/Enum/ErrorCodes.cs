﻿namespace BonTech.Product.Domain.Enum;

public enum ErrorCodes
{
    ProductNotFound = 1,
    ProductsNotFound = 2,
    ProductAlreadyExists = 3,
    ProductNotCreated = 4,
    ProductNotDeleted = 5,
    
    CategoryNotFound = 6,
    CategoryAlreadyExists = 7,
    
    OK = 200,
    InternalServerError = 500
}