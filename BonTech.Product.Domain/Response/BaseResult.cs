﻿using BonTech.Product.Domain.Enum;

namespace BonTech.Product.Domain.Response;

public class BaseResult : IBaseResult
{
    public string? ErrorMessage { get; set; }
    public int? ErrorCode { get; set; }
}

public class BaseResult<T> : IBaseResult<T>
{
    public BaseResult(string errorMessage, int errorCode, T data)
    {
        ErrorMessage = errorMessage;
        ErrorCode = errorCode;
        Data = data;
    }
    
    public BaseResult(string errorMessage, int errorCode)
    {
        ErrorMessage = errorMessage;
        ErrorCode = errorCode;
    }
    
    public BaseResult(T data, int errorCode)
    {
        ErrorCode = errorCode;
        Data = data;
    }

    public BaseResult() {  }
    
    public string? ErrorMessage { get; set; }
    public int? ErrorCode { get; set; }

    public T Data { get; set; }
}

public interface IBaseResult<T> : IBaseResult
{
    T Data { get; }
}

public interface IBaseResult
{
    bool IsSuccess => ErrorMessage == null;
    string? ErrorMessage { get; set; }
    int? ErrorCode { get; set; }
}