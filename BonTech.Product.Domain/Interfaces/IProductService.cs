﻿using BonTech.Product.Domain.Dto;
using BonTech.Product.Domain.Response;

namespace BonTech.Product.Domain.Interfaces;
/// <summary>
/// Сервис отвечающий за работу с доменной части Product
/// </summary>
public interface IProductService
{
    /// <summary>
    /// Получение всех продуктов
    /// </summary>
    /// <returns></returns>
    Task<IBaseResult<IEnumerable<ProductDto>>> GetProductsAsync();
    
    /// <summary>
    /// Получение продуктов по параметрам (название)
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    Task<IBaseResult<IEnumerable<OrderProductDto>>> GetProductsAsync(OrderDto dto);
    
    /// <summary>
    /// Получение продукта по идентификатору
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    Task<IBaseResult<ProductDto>> GetProductAsync(long id);
    
    /// <summary>
    /// Создание продукта с базовыми параметрами
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    Task<IBaseResult<ProductDto>> CreateProductAsync(ProductDto dto);
    
    /// <summary>
    /// Удаление продукта по идентификатору
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    Task<IBaseResult<bool>> DeleteProductAsync(long id);
    
    /// <summary>
    /// Обновление параметров продукта по идентификатору 
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    Task<IBaseResult<ProductDto>> UpdateProductAsync(ProductDto dto);
}