﻿using BonTech.Product.Domain.Dto;
using BonTech.Product.Domain.Response;

namespace BonTech.Product.Domain.Interfaces.Validations;

/// <summary>
/// Класс валидации отвечающий за продукт
/// </summary>
public interface IProductValidator
{
    /// <summary>
    /// Проверяется наличие продукта, если продукт с переданным названием есть в БД, то создать точно такой же нельзя
    /// Затем проверяется переданная категория, если её нет в БД, то продукт не будет создан
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    Task<IBaseResult> ValidateCreateAsync(ProductDto dto);

    /// <summary>
    /// Проверяется наличие продукта и категории по названию, если объекты есть в системе, то продукт будет обновлен
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    Task<IBaseResult> ValidateUpdateAsync(ProductDto dto);
    
    /// <summary>
    /// Проверяется продукт на налиичие, если продукт найден по Id, то он будет удален
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    Task<IBaseResult> ValidateDeleteAsync(long id);
}