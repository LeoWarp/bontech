﻿namespace BonTech.Product.Domain.Dto;

/// <summary>
/// Модель предназначенная для добавления и обновления сущности Product
/// </summary>
public class ProductDto
{
    public long Id { get; set; }
    
    public string? Name { get; set; }
    
    public string? Description { get; set; }
    
    public decimal Price { get; set; }
    
    public string? Category { get; set; }
}