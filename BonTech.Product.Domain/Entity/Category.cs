﻿namespace BonTech.Product.Domain.Entity;

public class Category
{
    public long Id { get; set; }
    
    public string? Name { get; set; }

    public Product Product { get; set; }
}