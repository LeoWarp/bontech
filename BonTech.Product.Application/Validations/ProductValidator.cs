﻿using BonTech.Product.Domain.Dto;
using BonTech.Product.Domain.Entity;
using BonTech.Product.Domain.Enum;
using BonTech.Product.Domain.Interfaces;
using BonTech.Product.Domain.Interfaces.Validations;
using BonTech.Product.Domain.Response;
using Microsoft.EntityFrameworkCore;

namespace BonTech.Product.Application.Validations;

/// <inheritdoc />
public class ProductValidator : IProductValidator
{
    private readonly IBaseRepository<Domain.Entity.Product> _productRepository;
    private readonly IBaseRepository<Category> _categoryRepository;

    public ProductValidator(IBaseRepository<Domain.Entity.Product> productRepository, IBaseRepository<Category> categoryRepository)
    {
        _productRepository = productRepository;
        _categoryRepository = categoryRepository;
    }

    /// <inheritdoc />
    public async Task<IBaseResult> ValidateCreateAsync(ProductDto dto)
    {
        var product = await _productRepository.GetAll().FirstOrDefaultAsync(x => x.Name == dto.Name);
        if (product != null)
        {
            return new BaseResult()
            {
                ErrorMessage = ErrorMessage.ProductAlreadyExists,
                ErrorCode = (int)ErrorCodes.ProductAlreadyExists
            };
        }
        var category = await _categoryRepository.GetAll().FirstOrDefaultAsync(x => x.Name == dto.Category);
        if (category == null)
        {
            return new BaseResult()
            {
                ErrorMessage = ErrorMessage.CategoryNotFound,
                ErrorCode = (int)ErrorCodes.CategoryNotFound
            };
        }
        return new BaseResult()
        {
            ErrorCode = (int)ErrorCodes.OK
        };
    }

    /// <inheritdoc />
    public async Task<IBaseResult> ValidateDeleteAsync(long id)
    {
        var product = await _productRepository.GetAll().FirstOrDefaultAsync(x => x.Id == id);
        if (product == null)
        {
            return new BaseResult()
            {
                ErrorMessage = ErrorMessage.ProductNotFound,
                ErrorCode = (int)ErrorCodes.ProductNotFound
            };
        }
        return new BaseResult()
        {
            ErrorCode = (int)ErrorCodes.OK
        };
    }

    /// <inheritdoc />
    public async Task<IBaseResult> ValidateUpdateAsync(ProductDto dto)
    {
        var product = await _productRepository.GetAll().FirstOrDefaultAsync(x => x.Id == dto.Id);
        if (product == null)
        {
            return new BaseResult()
            {
                ErrorMessage = ErrorMessage.ProductNotFound,
                ErrorCode = (int)ErrorCodes.ProductNotFound
            };
        }
        var category = await _categoryRepository.GetAll().FirstOrDefaultAsync(x => x.Name == dto.Category);
        if (category == null)
        {
            return new BaseResult()
            {
                ErrorMessage = ErrorMessage.CategoryNotFound,
                ErrorCode = (int)ErrorCodes.CategoryNotFound
            };
        }
        return new BaseResult()
        {
            ErrorCode = (int)ErrorCodes.OK
        };
    }
}