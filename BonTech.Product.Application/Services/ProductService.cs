﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using BonTech.Product.Domain.Dto;
using BonTech.Product.Domain.Entity;
using BonTech.Product.Domain.Enum;
using BonTech.Product.Domain.Interfaces;
using BonTech.Product.Domain.Interfaces.Validations;
using BonTech.Product.Domain.Response;
using Microsoft.EntityFrameworkCore;
using Serilog;
using ProductEntity = BonTech.Product.Domain.Entity.Product;

namespace BonTech.Product.Application.Services;

/// <inheritdoc />
public class ProductService : IProductService
{
    private readonly IBaseRepository<ProductEntity> _productRepository;
    private readonly IBaseRepository<Category> _categoryRepository;
    private readonly ILogger _logger;
    private readonly IMapper _mapper;
    private readonly IProductValidator _productValidator;

    public ProductService(IBaseRepository<ProductEntity> productRepository,
        IBaseRepository<Category> categoryRepository,
        ILogger logger, IMapper mapper, IProductValidator productValidator)
    {
        _productRepository = productRepository;
        _categoryRepository = categoryRepository;
        _logger = logger.ForContext<ProductService>();
        _mapper = mapper;
        _productValidator = productValidator;
    }

    /// <inheritdoc />
    public async Task<IBaseResult<IEnumerable<ProductDto>>> GetProductsAsync()
    {
        try
        {
            var products = await _productRepository.GetAll()
                .Include(x => x.Category)
                .Select(x => new ProductDto()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description,
                    Price = x.Price,
                    Category = x.Category.Name
                })
                .ToListAsync();

            return new BaseResult<IEnumerable<ProductDto>>()
            {
                Data = products,
                ErrorCode = (int)ErrorCodes.OK
            };
        }
        catch (Exception ex)
        {
            _logger.Warning(ex, "Продукты не найдены");
            return new BaseResult<IEnumerable<ProductDto>>()
            {
                ErrorMessage = ErrorMessage.ProductsNotFound,
                ErrorCode = (int)ErrorCodes.ProductsNotFound
            };
        }
    }
    
    /// <inheritdoc />
    public async Task<IBaseResult<IEnumerable<OrderProductDto>>> GetProductsAsync(OrderDto dto)
    {
        OrderProductDto[] products;
        try
        {
            products = await _productRepository.GetAll()
                .Include(x => x.Category)
                .Where(x => dto.Products.Contains(x.Name))
                .ProjectTo<OrderProductDto>(_mapper.ConfigurationProvider)
                .ToArrayAsync();
        }
        catch (Exception ex)
        {
            _logger.Warning(ex,$"Продукты не найдены: {ex.Message}", dto.Products.Count);
            return new BaseResult<IEnumerable<OrderProductDto>>()
            {
                ErrorMessage = ErrorMessage.ProductsNotFound,
                ErrorCode = ErrorCodes.ProductsNotFound as int?
            };
        }
        
        if (!products.Any())
        {
            _logger.Warning("Продукты не найдены", dto.Products.Count);
            return new BaseResult<IEnumerable<OrderProductDto>>()
            {
                ErrorMessage = ErrorMessage.ProductsNotFound,
                ErrorCode = (int)ErrorCodes.ProductsNotFound
            };
        }
        return new BaseResult<IEnumerable<OrderProductDto>>
        {
            Data = products,
            ErrorCode = (int)ErrorCodes.OK
        };
    }

    public async Task<IBaseResult<ProductDto>> GetProductAsync(long id)
    {
        try
        {
            var product = await _productRepository.GetAll()
                .Include(x => x.Category)
                .Select(x => new ProductDto()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description,
                    Price = x.Price,
                    Category = x.Category.Name
                })
                .FirstOrDefaultAsync(x => x.Id == id);

            if (product == null)
            {
                return new BaseResult<ProductDto>()
                {
                    ErrorMessage = ErrorMessage.ProductNotFound,
                    ErrorCode = (int)ErrorCodes.ProductNotFound
                };
            }
            return new BaseResult<ProductDto>()
            {
                Data = product,
                ErrorCode = (int)ErrorCodes.OK
            };
        }
        catch (Exception ex)
        {
            _logger.Error(ex, $"Продукт с {id} не найден", id);
            return new BaseResult<ProductDto>()
            {
                ErrorMessage = ErrorMessage.ProductNotFound,
                ErrorCode = (int)ErrorCodes.ProductNotFound
            };
        }
    }

    public async Task<IBaseResult<ProductDto>> CreateProductAsync(ProductDto dto)
    {
        try
        {
            var result = await _productValidator.ValidateCreateAsync(dto);
            if (!result.IsSuccess)
            {
                return new BaseResult<ProductDto>()
                {
                    ErrorMessage = result.ErrorMessage,
                    ErrorCode = result.ErrorCode
                };
            }
            
            var product = await _productRepository.GetAll().FirstOrDefaultAsync(x => x.Name == dto.Name);
            var category = await _categoryRepository.GetAll().FirstOrDefaultAsync(x => x.Name == dto.Category);
            product = new ProductEntity()
            {
                Name = dto.Name,
                Description = dto.Description,
                Price = dto.Price,
                Category = category
            };
            await _productRepository.CreateAsync(product);
            await _productRepository.SaveChangesAsync();

            return new BaseResult<ProductDto>()
            {
                Data = _mapper.Map<ProductDto>(product),
                ErrorCode = (int)ErrorCodes.OK
            };
        }
        catch (Exception ex)
        {
            _logger.Error(ex, $"Продукт не был создан", dto);
            return new BaseResult<ProductDto>()
            {
                ErrorMessage = ErrorMessage.ProductNotCreated,
                ErrorCode = (int)ErrorCodes.ProductNotCreated
            };
        }
    }

    public async Task<IBaseResult<bool>> DeleteProductAsync(long id)
    {
        try
        {
            var result = await _productValidator.ValidateDeleteAsync(id);
            if (!result.IsSuccess)
            {
                return new BaseResult<bool>()
                {
                    ErrorMessage = result.ErrorMessage,
                    ErrorCode = result.ErrorCode
                };
            }
            var product = await _productRepository.GetAll().FirstOrDefaultAsync(x => x.Id == id);

            await _productRepository.RemoveAsync(product);
            await _productRepository.SaveChangesAsync();

            return new BaseResult<bool>()
            {
                ErrorCode = (int)ErrorCodes.OK
            };
        }
        catch (Exception ex)
        {
            _logger.Error(ex, "Продукт не был удален", id);
            return new BaseResult<bool>()
            {
                ErrorMessage = ErrorMessage.ProductNotDeleted,
                ErrorCode = (int)ErrorCodes.ProductNotDeleted
            };
        }
    }

    public async Task<IBaseResult<ProductDto>> UpdateProductAsync(ProductDto dto)
    {
        try
        {
            var result = await _productValidator.ValidateUpdateAsync(dto);
            if (!result.IsSuccess)
            {
                return new BaseResult<ProductDto>()
                {
                    ErrorMessage = result.ErrorMessage,
                    ErrorCode = result.ErrorCode
                };
            }
            
            var product = await _productRepository.GetAll().FirstOrDefaultAsync(x => x.Id == dto.Id);
            var category = await _categoryRepository.GetAll().FirstOrDefaultAsync(x => x.Name == dto.Category);

            product.Description = dto.Description;
            product.Name = dto.Name;
            product.Price = dto.Price;
            product.Category = category;

            await _productRepository.UpdateAsync(product);
            await _productRepository.SaveChangesAsync();

            return new BaseResult<ProductDto>()
            {
                ErrorCode = (int)ErrorCodes.OK
            };
        }
        catch (Exception ex)
        {
            _logger.Error(ex, "Продукт не был обновлен", dto);
            return new BaseResult<ProductDto>()
            {
                ErrorMessage = ErrorMessage.ProductNotDeleted,
                ErrorCode = (int)ErrorCodes.ProductNotDeleted
            };
        }
    }
}