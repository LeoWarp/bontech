using BonTech.Product.Application.Mapping;
using BonTech.Product.Application.Services;
using BonTech.Product.Domain.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BonTech.Product.Application.DependancyInjection
{
    /// <summary>
    /// Внедрение зависимостей слоя Application.
    /// </summary>
    public static class DependencyInjection
    {
        public static void AddApplication(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAutoMapper(typeof(ProductMapping));

            services.ServicesInit();
        }

        private static void ServicesInit(this IServiceCollection services)
        {
            services.AddScoped<IProductService, ProductService>();
        }
    }
}